<!DOCTYPE html>
<html>
	<head>
        <style>
			.container {
            	height: 370px;
            }

            #local, #remote {
                border-style: solid;
                float: left;
                margin-right: 5px;
            }
            
            #local {
                width: 320px;
                height: 240px;
                margin-top: 120px;
            }
            
            #remote {
                width: 480px;
                height: 360px;

            }
            
        </style>
    </head>
    <body>
    	<div class="container">
	        <div id='remote'></div>
	        <div id='local'></div>
        </div>
        <form id='dial'>
            <input type='text' name='number'></input>
            <button type='submit'>Dial</button>
            <button id='hangup'>Hangup</button>
        </form>
    </body>

	<!-- Import the FCSDK JavaScript libraries -->
	<script type='text/javascript' src='http://fusion.vbox:8080/gateway/adapter.js'></script>
	<script type='text/javascript' src='http://fusion.vbox:8080/gateway/fusion-client-sdk.js'></script>

	<!-- Although not required by the FCSDK, jQuery will ease interactions with the DOM -->
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>

	<script>
		<?php 
			// import our init script & provision a session
			include_once('init.php');
			$token = provision('nathan', 'abc');
		?>
		
		var token = <?= $token ?>;

		// initialise the connection to the Gateway
		UC.start(token.sessionid);

		// when available, direct the local media stream to the right DOM element
	    var local = $('#local')[0];
	    UC.phone.setPreviewElement(local);

	    // wire up the call handler
	    $('#dial').submit(function (event) {
		    event.preventDefault();
		    var number = $(this).find('[name=number]').val();

		    // create the call
		    var call = UC.phone.createCall(number);
		    
		    // set the remote video element
		    var remote = $('#remote')[0];
		    call.setVideoElement(remote);
		    
		    // now dial the call
		    call.dial();
		});

	    // wire up the hangup handler
		$('#hangup').click(function(event) {
			// prevent the button from causing the form to submit
			event.preventDefault();

		    // if we're on a call - end it!
		    var calls = UC.phone.getCalls();
		    var call = calls[0];
		    if (call) {
		        call.end();
		    }
		});
	</script>
</html>